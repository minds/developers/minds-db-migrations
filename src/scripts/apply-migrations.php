<?php

use Dotenv\Dotenv;
use Minds\MindsDbMigration\Databases\MySql\Client as MySqlClient;

require_once __DIR__ . '/../../vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
$dotenv->load();

$mysqlClient = new MySqlClient();

$mysqlClient->processMigrations();
